package util;

import java.util.Collection;

import com.orientechnologies.orient.core.db.record.OTrackedList;
import com.orientechnologies.orient.core.type.tree.OMVRBTreeRIDSet;

/**
 *  
 * @author kb
 *
 */
public class Count {

	/**
	 * Utility class which returns the size of several collections: Object[] long[] OTrackedList<?>  OMVRBTreeRIDSet Collection<?>
	 * @param collection
	 * @return integer size of collection
	 */
		public int count(Object collection) {

			if (collection instanceof Object[]) {

				return ((Object[]) collection).length;

			}

			else if (collection instanceof long[]) {

				return ((long[]) collection).length;

			} 
			
			else if (collection instanceof OTrackedList<?>) {
				
				return ((OTrackedList<?>) collection).size();
				
			}
			
			else if (collection instanceof OMVRBTreeRIDSet) {
				
				return ((OMVRBTreeRIDSet) collection).size();
				
			}
			
			else if (collection instanceof Collection<?>) {

				return ((Collection<?>) collection).size();
				
			}else{ 
					if(!(collection==null))
					System.err.println("count failed! "+collection.getClass());
					return 0;
					
				}
					

		}


	}

	

