package tests;

import java.io.File;
import java.util.HashSet;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import util.ParseResource;

import com.orientechnologies.orient.core.config.OGlobalConfiguration;
import com.orientechnologies.orient.core.intent.OIntentMassiveInsert;
//import com.orientechnologies.orient.core.db.graph.OGraphDatabase;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.tx.OTransaction.TXTYPE;

import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import com.tinkerpop.blueprints.util.wrappers.batch.BatchGraph;

/**
 * creates a database with the input xmi file (in args[0]) or reads it if the
 * database exists; and provides debugging information
 * 
 */
public class OrientDBSaveTests {

	String system = System.getProperty("user.dir").replaceAll("\\\\", "/");
	String db = system + "/graph";

	private ResourceSet modelResourceSet; // model and metamodel resource set
	private File f; // xmi file to import model from
	private OrientGraph graph; // database
	private HashSet<File> files; // metamodel file(s)
	private HashSet<File> movedfiles; // metamodel file(s) - after moved to the
	// database
	private File logfile; // log file
	private File indexlogfile; // index log file

	private Resource metamodelResource; // metamodel resource

	@SuppressWarnings("unused")
	private int unset = 0; // the number of unset references

	/**
	 * xmiFileToConvert: the String URI of the xmi file to convert to orient (
	 * for example D://workspace/OrientDBstore/model/set0.xmi ) the folder must
	 * also contain the valid .ecore metamodel(s) of the xmi file
	 * 
	 * @param xmiFileToConvert
	 */
	public static void main(String[] args) {

		new OrientDBSaveTests().run();

	}

	private void run() {

		JFrame window = null;
		try {

			// create resource set and add xmi and ecore packages
			modelResourceSet = new ResourceSetImpl();
			modelResourceSet.getResourceFactoryRegistry()
					.getExtensionToFactoryMap()
					.put("xmi", new XMIResourceFactoryImpl());
			modelResourceSet.getResourceFactoryRegistry()
					.getExtensionToFactoryMap()
					.put("ecore", new EcoreResourceFactoryImpl());

			window = new JFrame();

			JFileChooser filechoser = new JFileChooser();
			filechoser.setDialogTitle("Chose Model File:");

			File temp = new File("");
			String parent = temp.getAbsolutePath().replaceAll("\\\\", "/");

			filechoser.setCurrentDirectory(new File(parent + "/model/"));
			FileNameExtensionFilter filter = new FileNameExtensionFilter("XMI",
					"xmi");
			filechoser.setFileFilter(filter);

			if (filechoser.showDialog(window, "Select File") == JFileChooser.APPROVE_OPTION)
				f = filechoser.getSelectedFile();
			else {
				System.err
						.println("Chosing of model file canceled, program terminated");
				System.exit(1);
			}

			db+="-"+f.getName();
			
			db = system + "/moo-t4";
			
			System.out.println(db);
			
			files = new HashSet<File>();

			movedfiles = new HashSet<File>();

			@SuppressWarnings("unused")
			long cpu = System.nanoTime();

			boolean isNew = false;
			OrientGraphFactory factory = new OrientGraphFactory("plocal:" + db);
			//graph = factory.getTx();
			graph = factory.getTx();
			graph.getRawGraph().declareIntent(new OIntentMassiveInsert());
			graph.getRawGraph().begin(TXTYPE.OPTIMISTIC);

			graph.setUseLightweightEdges(false);
			graph.setUseClassForEdgeLabel(false);
			graph.setUseClassForVertexLabel(false);
			graph.setUseVertexFieldsForEdgeLabels(false);
			
				System.out.println("Creating: "
						+ db
						+ "\nWITH: "
						+ Runtime.getRuntime().maxMemory()
						/ 1000000000
						+ "."
						+ Runtime.getRuntime().maxMemory()
						% 1000000000
						+ " GB of HEAP and: "
						+ OGlobalConfiguration.FILE_MMAP_MAX_MEMORY
								.getValueAsLong()
						/ 1000000000
						+ "."
						+ OGlobalConfiguration.FILE_MMAP_MAX_MEMORY
								.getValueAsLong() % 1000000000
						+ " GB of Database MMAP");
				isNew = true;
				/*
				graph.command(
						new OCommandSQL(
								"CREATE INDEX metacdictionary dictionary String"))
						.execute();*/
			registerShutdownHook(graph);


			// create log and indexlog files
			logfile = new File("log.txt");
			try {
				if (!logfile.exists())
					logfile.createNewFile();
			} catch (Exception ee) {
				ee.printStackTrace();
			}
			indexlogfile = new File("indexlog.txt");
			try {
				if (!indexlogfile.exists())
					indexlogfile.createNewFile();
			} catch (Exception ee) {
				ee.printStackTrace();
			}

			// new database actions
			if (isNew) {

				// MASSIVE BUG, BREAKS REFERENCING!!! - caused by using ORID
				// referencing without a lvl1cache
				
				// create root node (currently no useful info in it)
				Vertex rootNode = graph.addVertex(null);
				rootNode.setProperty("root","true");
				graph.commit();

				// copy the metamodel files to the database folder (for faster
				// access should the model be in a remote repository)
				for (File metamodelFile : f.getParentFile().listFiles()) {
					if (metamodelFile.getAbsolutePath().endsWith(".ecore")) {
						files.add(metamodelFile);

					}
				}
				for (File metamodelFile : files) {
					File destfile = new File(db + "/" + metamodelFile.getName());
					new util.CopyFile().copyFile(metamodelFile, destfile);
					movedfiles.add(destfile);
				}
				System.out.println("metamodel copied to database...");

				// register the EPackages used by this graph.
				for (File metamodelFile : movedfiles) {

					files.add(metamodelFile);
					metamodelResource = modelResourceSet.getResource(
							URI.createFileURI(metamodelFile.getAbsolutePath()),
							true);

					for (EObject eObject : metamodelResource.getContents()) {

						if (eObject instanceof EPackage)
							new util.RegisterMeta()
									.registerPackages((EPackage) eObject);

					}
				}
				System.out.println("metamodel registered...");

				// populate the database
				addnodes();

			}

			// log graph and index
			// System.out.println(new util.ToLog().toLog(graph, logfile,
			// unset));
			// new util.indexLog().log(graph, indexlogfile);

			// print information about the contents of the graph
			/*
			int element_count = ((List<?>) graph.command(
					new OCommandSQL("select * from index:metacdictionary"))
					.execute()).size();

			System.out.print("Metamodel Class Dictionary contains: ");

			System.out.println(element_count
					+ " entries ( unique-uri-id : nodeid )");

			//
			System.out.print("Dictionary contains: ");

			element_count = ((List<?>) graph.command(
					new OCommandSQL("select * from index:dictionary"))
					.execute()).size();

			System.out.println(element_count
					+ " entries ( unique-uri-id : nodeid )");

			System.out
					.println("\nProgram ending with no errors, shutting down database...");*/

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			long init = System.nanoTime();
			graph.shutdown();
			System.out.println("(took ~" + (System.nanoTime() - init)
					/ 1000000000 + "sec to commit changes)");
			window.dispose();
		}

	}

	/**
	 * Populates the database with the model, using util.parseresource
	 * 
	 */
	private void addnodes() {

		long init = System.nanoTime();

		Resource modelResource = modelResourceSet.getResource(
				URI.createFileURI(f.getAbsolutePath()), true);

		System.out.println("model resource loaded, (took ~"
				+ (System.nanoTime() - init) / 1000000000 + "sec)");

		unset = new ParseResource(graph, movedfiles, modelResource,
				modelResourceSet).getUnset();

		init = System.nanoTime();

		try {
			modelResource.unload();
			System.gc();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("resource unloaded (took ~"
				+ (System.nanoTime() - init) / 1000000000 + "sec)\n");
	}

	/**
	 * adds the metamodel to the database using util.pareresource, for debugging
	 * or future use
	 * 
	 */
	@SuppressWarnings("unused")
	private void addmeta() {

		// new OrientParseResource().parseResource(3, metamodelResource, graph,
		// hash);
		// new OrientParseResource().parseResource(4, metamodelResource, graph,
		// hash);

	}

	/**
	 * safety shutdownhook for JVM interruption
	 * 
	 * @param database
	 */
	private void registerShutdownHook(final OrientGraph database) {

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					long l = System.nanoTime();
					database.shutdown();
					System.out.println("SHUTDOWN HOOK INVOKED: (took ~"
							+ (System.nanoTime() - l) / 1000000000
							+ "sec to commit changes)");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
