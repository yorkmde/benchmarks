package tests;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Index;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

public class SimpleTest {

	public static void main(String[] args) {
		String system = System.getProperty("user.dir").replaceAll("\\\\", "/");
		String db = system + "/graph";
		System.out.println(db);
		OrientGraph graph = new OrientGraph("plocal:" + db);
		graph.setUseLightweightEdges(false);
		graph.drop();
		
		graph = new OrientGraph("plocal:" + db);
		graph.setUseLightweightEdges(false);
		
		Vertex v1 = graph.addVertex(null);
		v1.setProperty("name", "William");
		
		Vertex v2 = graph.addVertex(null);
		v2.setProperty("name", "Peter");
		
		graph.commit();
		Index<Vertex> indexVertex = graph.createIndex("people", Vertex.class, null);
		indexVertex.put("name", "people", v1);
		indexVertex.put("name", "people", v2);

		
		Index<Edge> indexEdge = graph.createIndex("relationship", Edge.class, null);
		Edge e1 = graph.addEdge(null, v2, v1, "knows/s");
		indexEdge.put("name", "knows", e1);

		//graph.commit();
		
		for(Vertex v : indexVertex.get("name", "people"))
		{
			System.out.println("=====" + v.getProperty("name"));
		}
		
		for(Edge e: indexEdge.get("name", "knows"))
		{
			System.out.println("=====>" + e.getLabel());
		}
			
		/*
		for(Vertex v: graph.getVertices())
		{
			graph.removeVertex(v);
		}
		for(Edge e: graph.getEdges())
		{
			graph.removeEdge(e);
		}*/
		
		/*
		Vertex v1 = graph.addVertex(null);
		
		Vertex v2 = graph.addVertex(null);
		v2.setProperty("name", "Whoever");
		
		
		graph.commit();
		
		for(Vertex v: graph.getVertices())
		{
			System.out.println(v.getProperty("name"));
		}
		for(Edge e: graph.getEdges())
		{
			System.out.println(e.getLabel());
		}*/
		
		graph.shutdown();
	}
}
