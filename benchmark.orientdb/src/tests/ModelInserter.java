package tests;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

public class ModelInserter {

	OrientGraph graph;
	
	private boolean addMetaClass(EObject eClass) {

		if (eClass instanceof EClass) {

			String id = getEObjectId((EClass)eClass);
			
			Vertex n = createEClassNode(eClass, id);

			return true;
		} else
			return false;
	}
	private Vertex createEClassNode(EObject eObject, String id) {

		Vertex node = graph.addVertex(null);
		node.setProperty("eObjectId", id);
		
		// n.field("nUri", ((EClass)child).getEPackage()
		// .getNsURI());

		
		return node;
	}
	private String getEObjectId(EClass eClass) {
		return eClass.getEPackage().getNsURI() + "/" + eClass.getName();
	}

}
