package impl;


import accumulograph.AccumuloGraph;
import accumulograph.AccumuloGraphOptions;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
public class TestClass {

	public static void main(String[] args) {
		try {
			  String graphName = "MyFirstGraph";
			  String vertices = "MyVerticesCollection";
			  String edges = "MyEdgesCollection";

			  // cleate a ArangoDB graph
				AccumuloGraphOptions opts = new AccumuloGraphOptions();
				String instanceName = "myinstance";
				String zooServers = "zooserver-one,zooserver-two";
				opts.setConnectorInfo(instanceName, zooServers, "", "");
				// OR
				//opts.setConnector(connector);

				opts.setGraphTable("bluprints");

				// Optional
				//opts.setIndexTable(indexTable);
				opts.setAutoflush(false);
				//opts.setReturnRemovedPropertyValues(...);
				opts.setMock(true);

				
				Graph graph = new AccumuloGraph(opts);
			  
			  Vertex v1 =  graph.addVertex(null);

			  v1.setProperty("name", "William's node");
			  
			  System.out.println(v1.getId().toString());
			  Vertex v2 = graph.addVertex(null);

			  Vertex v = graph.getVertices("name", "William's node").iterator().next();
			  
			  System.out.println(v.toString());
			  // create edge
			  Edge e1 = graph.addEdge("e1", v1, v2, "knows");

			  // close the graph
			  graph.shutdown();
			} catch (Exception e) {
			  e.printStackTrace();
			  //fail(e.getMessage());
			}
	}
	
}
