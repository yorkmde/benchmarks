package impl;


import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.sail.impls.NativeStoreSailGraph;

public class TestClass {

	public static void main(String[] args) {
		try {
			  String graphName = "MyFirstGraph";
			  String vertices = "MyVerticesCollection";
			  String edges = "MyEdgesCollection";

			  // cleate a ArangoDB graph
			  NativeStoreSailGraph graph = new NativeStoreSailGraph("file/");
		        graph.addNamespace("tg", "http://tinkerpop.com#"); 
			  
			  Vertex v1 = (Vertex) graph.addVertex("http://something");

			  Vertex v3 = (Vertex) graph.addVertex("\"William's node\"");
			  
			  Edge e1 = graph.addEdge("e1", v1, v3, "http://name");

			  
			  System.out.println(v1.getId().toString());
			  Vertex v2 = graph.addVertex(null);

			  ///Vertex v = graph.getVertices("name", "William's node").iterator().next();
			  
			  //System.out.println(v.toString());
			  // create edge
			  Edge e2 = graph.addEdge("e2", v1, v2, "http://tinkerpop.com#knows");

			  // close the graph
			  graph.shutdown();
			} catch (Exception e) {
			  e.printStackTrace();
			  //fail(e.getMessage());
			}
	}
	
}
