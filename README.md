# Instructions#

Initial instructions. This process will be made simpler using a maven build script.

### Step One ###

Download Eclipse+EMF and make sure it runs. This project has been tested with the EMF bundles for Juno and Kepler, on Java 1.7.

### Step Two ###

Use your git client to clone the framework (N.B. ~150MB):

> git clone --depth 0 https://bitbucket.org/yorkmde/benchmarks.git
(this ignore deleted files)

This folder is an Eclipse workspace, each project of the workspace contains a harness for different database. The libs project is a central location for Java and dataset dependencies.

To run the code, switch workspaces in Eclipse to the 'benchmarks' folder and import each of the subfolders as an Eclipse project- use the "Java from existing sources..." menu and the option "without copying files into workspace". 

Keeping this structure helps resolve dependency and file path issues in step three and four.

### Step Three ###

Download the datasets from:

> [http://docatlanmod.emn.fr/GraBaTs/all.zip](http://docatlanmod.emn.fr/GraBaTs/all.zip)
> [http://www.emn.fr/z-info/atlanmod/index.php/GraBaTs_2009_Case_Study](http://www.emn.fr/z-info/atlanmod/index.php/GraBaTs_2009_Case_Study)

and the place files set(0-4).xmi and the three ecore meta models in the 'benchmarks/libs/models' folder.

### Step Four ###

Download or build the library and driver JARs for your chosen benchmarks and place them in the libs/libs tree. 

If the drivers are placed in the correct sub-folders and name as described in:

> 'benchmarks/libs/library.list.DOWNLOADME.txt'

then the Eclipse projects will build without changes to the build-path properties. If this is not possible, the Eclipse build configuration for each project will need to be updated to reflect your library locations.
