package tests;

import be.datablend.blueprints.impls.mongodb.MongoDBGraph;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Index;
import com.tinkerpop.blueprints.Vertex;

public class SimpleTest {

	public static void main(String[] args) {
		String system = System.getProperty("user.dir").replaceAll("\\\\", "/");
		String db = system + "/graph";
		System.out.println(db);
		MongoDBGraph graph = new MongoDBGraph("127.0.0.1", 27017);
		
		graph.clear();
		
		Vertex v1 = graph.addVertex(null);
		v1.setProperty("name", "William");
		
		Vertex v2 = graph.addVertex(null);
		v2.setProperty("name", "Peter");
		
		//Index<Vertex> indexVertex = graph.createIndex("people", Vertex.class, null);
		//indexVertex.put("name", "people", v1);
		//indexVertex.put("name", "people", v2);

		
		//Index<Edge> indexEdge = graph.createIndex("relationship", Edge.class, null);
		Edge e1 = graph.addEdge(null, v2, v1, "knows/s");
		//indexEdge.put("name", "knows", e1);

		//graph.commit();
		
		/*
		for(Vertex v: graph.getVertices())
		{
			graph.removeVertex(v);
		}
		for(Edge e: graph.getEdges())
		{
			graph.removeEdge(e);
		}*/
		
		/*
		Vertex v1 = graph.addVertex(null);
		
		Vertex v2 = graph.addVertex(null);
		v2.setProperty("name", "Whoever");
		
		
		graph.commit();
		*/
		for(Vertex v: graph.getVertices())
		{
			System.out.println(v.getProperty("name"));
		}
		for(Edge e: graph.getEdges())
		{
			System.out.println(e.getLabel());
		}
		
		graph.shutdown();
	}
}
