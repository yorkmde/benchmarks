package impl;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBInit {
	
	  private static Connection connect = null;
	  private static Statement statement = null;
	  private static ResultSet resultSet = null;


	public static void main(String[] args) throws ClassNotFoundException, SQLException {
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager
	          .getConnection("jdbc:mysql://localhost/set0?"
	              + "user=root&password=root");

	      // Statements allow to issue SQL queries to the database
	      statement = connect.createStatement();
	      // Result set get the result of the SQL query
	    //  statement.executeUpdate("drop database blueprints");
	     // statement.executeUpdate("create database blueprints");
	      statement.executeUpdate("drop database set0");
	      statement.executeUpdate("create database set0");
	}
	
}
