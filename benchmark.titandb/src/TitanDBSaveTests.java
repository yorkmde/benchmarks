

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import util.ParseResource;


import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
/**
 * creates a database with the input xmi file (in args[0]) or reads it if the
 * database exists; and provides debugging information
 * 
 */
public class TitanDBSaveTests {

	String system = System.getProperty("user.dir").replaceAll("\\\\", "/");
	String db = system + "/graph";

	private ResourceSet modelResourceSet; // model and metamodel resource set
	private File f; // xmi file to import model from
	private TitanGraph graph; // database
	private HashSet<File> files; // metamodel file(s)
	private HashSet<File> movedfiles; // metamodel file(s) - after moved to the
	// database
	private File logfile; // log file
	private File indexlogfile; // index log file

	private Resource metamodelResource; // metamodel resource

	@SuppressWarnings("unused")
	private int unset = 0; // the number of unset references
	

	/**
	 * xmiFileToConvert: the String URI of the xmi file to convert to orient (
	 * for example D://workspace/OrientDBstore/model/set0.xmi ) the folder must
	 * also contain the valid .ecore metamodel(s) of the xmi file
	 * 
	 * @param xmiFileToConvert
	 */
	public static void main(String[] args) {

		new TitanDBSaveTests().run();
		//new TitanDBSaveTests().runQuery();
	}
	
	String dataset="set1";
	private void runQuery()
	{
		long init = System.nanoTime();
		graph = TitanFactory.open("../libs/databases/titan-db/"+dataset);

		System.out.println("(took ~" + (System.nanoTime() - init)
				/ 1000000000 + "secs to load graph)");
		
		init = System.nanoTime();
		int numberOfV = 0;
		int numberOfE = 0;
		for(Vertex v: graph.getVertices())
		{
			numberOfV++;
		}
		for(Edge e: graph.getEdges())
		{
			numberOfE++;
		}
		
		System.out.println("(took ~" + (System.nanoTime() - init)
				/ 1000000000 + "sec to traverse)");
		System.out.println("number of nodes: " + numberOfV + ", and number of edges: " + numberOfE);
	}

	private void run() {
		
		try {
			graph = TitanFactory.open("../libs/databases/titan-db/"+dataset);
			
			//Configuration conf = new BaseConfiguration();
			//conf.setProperty("storage.backend", "hbase");
			//conf.setProperty("storage.backend", "cassandra");
			//conf.setProperty("storage.hostname", "127.0.0.1");
			//conf.setProperty("storage.directory", "/tmp/titan");
			//graph = TitanFactory.open(conf);
			/*
			for(Vertex v: graph.getVertices())
			{
				graph.removeVertex(v);
			}
			
			for(Edge e: graph.getEdges())
			{
				graph.removeEdge(e);
			}*/
			
			// create resource set and add xmi and ecore packages
			modelResourceSet = new ResourceSetImpl();
			modelResourceSet.getResourceFactoryRegistry()
					.getExtensionToFactoryMap()
					.put("xmi", new XMIResourceFactoryImpl());
			modelResourceSet.getResourceFactoryRegistry()
					.getExtensionToFactoryMap()
					.put("ecore", new EcoreResourceFactoryImpl());


			f = new File("../libs/models/"+dataset+".xmi");


			db+="-"+f.getName();
			
			db = system + "/moo-t4";
			
			files = new HashSet<File>();

			movedfiles = new HashSet<File>();

			@SuppressWarnings("unused")
			long cpu = System.nanoTime();

			boolean isNew = false;
			
			
			isNew = true;
				/*
				graph.command(
						new OCommandSQL(
								"CREATE INDEX metacdictionary dictionary String"))
						.execute();*/
			registerShutdownHook(graph);


			// create log and indexlog files
			logfile = new File("log.txt");
			try {
				if (!logfile.exists())
					logfile.createNewFile();
			} catch (Exception ee) {
				ee.printStackTrace();
			}
			indexlogfile = new File("indexlog.txt");
			try {
				if (!indexlogfile.exists())
					indexlogfile.createNewFile();
			} catch (Exception ee) {
				ee.printStackTrace();
			}

			// new database actions
			if (isNew) {
				
				// create root node (currently no useful info in it)
				Vertex rootNode = graph.addVertex(null);
				rootNode.setProperty("root","true");
				graph.commit();

				// copy the metamodel files to the database folder (for faster
				// access should the model be in a remote repository)
				for (File metamodelFile : f.getParentFile().listFiles()) {
					if (metamodelFile.getAbsolutePath().endsWith(".ecore")) {
						files.add(metamodelFile);

					}
				}
				for (File metamodelFile : files) {
					File destfile = new File(db + "/" + metamodelFile.getName());
					new util.CopyFile().copyFile(metamodelFile, destfile);
					movedfiles.add(destfile);
				}
				System.out.println("metamodel copied to database...");

				// register the EPackages used by this graph.
				for (File metamodelFile : movedfiles) {

					files.add(metamodelFile);
					metamodelResource = modelResourceSet.getResource(
							URI.createFileURI(metamodelFile.getAbsolutePath()),
							true);

					for (EObject eObject : metamodelResource.getContents()) {

						if (eObject instanceof EPackage)
							new util.RegisterMeta()
									.registerPackages((EPackage) eObject);

					}
				}
				System.out.println("metamodel registered...");

				// populate the database
				addnodes();

			}

			// log graph and index
			// System.out.println(new util.ToLog().toLog(graph, logfile,
			// unset));
			// new util.indexLog().log(graph, indexlogfile);

			// print information about the contents of the graph
			/*
			int element_count = ((List<?>) graph.command(
					new OCommandSQL("select * from index:metacdictionary"))
					.execute()).size();

			System.out.print("Metamodel Class Dictionary contains: ");

			System.out.println(element_count
					+ " entries ( unique-uri-id : nodeid )");

			//
			System.out.print("Dictionary contains: ");

			element_count = ((List<?>) graph.command(
					new OCommandSQL("select * from index:dictionary"))
					.execute()).size();

			System.out.println(element_count
					+ " entries ( unique-uri-id : nodeid )");

			System.out
					.println("\nProgram ending with no errors, shutting down database...");*/

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			long init = System.nanoTime();
			graph.shutdown();
			System.out.println("(took ~" + (System.nanoTime() - init)
					/ 1000000000 + "sec to commit changes)");

		}

	}

	/**
	 * Populates the database with the model, using util.parseresource
	 * 
	 */
	private void addnodes() {

		long init = System.nanoTime();

		Resource modelResource = modelResourceSet.getResource(
				URI.createFileURI(f.getAbsolutePath()), true);

		System.out.println("model resource loaded, (took ~"
				+ (System.nanoTime() - init) / 1000000000 + "sec)");

		unset = new ParseResource(graph, movedfiles, modelResource,
				modelResourceSet).getUnset();

		init = System.nanoTime();

		try {
			//System.gc();
			modelResource.unload();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("resource unloaded (took ~"
				+ (System.nanoTime() - init) / 1000000000 + "sec)\n");
	}

	/**
	 * adds the metamodel to the database using util.pareresource, for debugging
	 * or future use
	 * 
	 */
	@SuppressWarnings("unused")
	private void addmeta() {

		// new OrientParseResource().parseResource(3, metamodelResource, graph,
		// hash);
		// new OrientParseResource().parseResource(4, metamodelResource, graph,
		// hash);

	}

	/**
	 * safety shutdownhook for JVM interruption
	 * 
	 * @param database
	 */
	private void registerShutdownHook(final TitanGraph database) {

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					long l = System.nanoTime();
					database.shutdown();
					System.out.println("SHUTDOWN HOOK INVOKED: (took ~"
							+ (System.nanoTime() - l) / 1000000000
							+ "sec to commit changes)");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void runGrabatsQuery() {
//		graph = TitanFactory.open("/tmp/titan");
		Configuration conf = new BaseConfiguration();
		//conf.setProperty("storage.backend", "hbase");
		conf.setProperty("storage.backend", "cassandra");
		conf.setProperty("storage.hostname", "127.0.0.1");
		conf.setProperty("storage.directory", "/tmp/titan");
		graph = TitanFactory.open(conf);
		Long startTime = System.nanoTime();
		int numberofVertexVisited = 0;

		// ------------------------------------------------------------------
		/*
		 * grabats query! Tests the Grabats query against an XMI file.
		 * 
		 * The query finds all classes (TypeDeclaration) that declare static
		 * public methods (MethodDeclaration) whose return type is the same
		 * class
		 */

		// ipublic+"(public) - "+istatic+"(static) - "+temp+"(public+static) - "+temp2+"(same
		// return type)

		int number = 0;
		HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
		for(Vertex vertex: graph.getVertices("eObjectID", "org.amma.dsl.jdt.dom/TypeDeclaration"))
		{
			numberofVertexVisited++;
			if (numberofVertexVisited % 100 == 0) {
				System.out.print(".");
			}
			String name = "";
			
			for(Edge vEdge: vertex.query().edges())
			{
				if(vEdge.getLabel().equals("_bodyDeclarations_"))
				{
					Vertex bodyVert = vEdge.getVertex(Direction.IN);
					String methodName = "";
					if (bodyVert.getProperty("eObjectID").equals("org.amma.dsl.jdt.dom/MethodDeclaration")) {
						boolean _static = false;
						boolean _public = false;
						for(Edge methodE : bodyVert.query().edges())
						{
							if (methodE.getLabel().equals("_modifiers_")) {
								Vertex modifierV = methodE.getVertex(Direction.IN);
								if (modifierV.getProperty("public") != null) {
									if (modifierV.getProperty("public").equals("true")) {
										_public = true;
										continue;
									}
								}
								if (modifierV.getProperty("static") != null) {
									if (modifierV.getProperty("static").equals("true")) {
										_static = true;
										continue;
									}
								}
							}
							if (methodE.getLabel().equals("_name_")) {
								Vertex methodNameV = methodE.getVertex(Direction.IN);
								if (methodNameV.getProperty("eObjectID").equals("org.amma.dsl.jdt.dom/SimpleName")) {
									methodName = methodNameV.getProperty("fullyQualifiedName");
								}
							}
						}
						outerloop:
						for(Edge methodE : bodyVert.query().edges())
						{
							if (!(_public && _static)) {
								break;
							}
							if (methodE.getLabel().equals("_returnType_")) {
								Vertex returnTypeV = methodE.getVertex(Direction.IN);
								for(Edge returnTypeEdge: returnTypeV.query().edges())
								{
									if (returnTypeEdge.getLabel().equals("_name_")) {
										Vertex nameVertex = returnTypeEdge.getVertex(Direction.IN);
										if (nameVertex.getProperty("fullyQualifiedName") != null) {
											for(Edge typeNameEdge: vertex.query().edges())
											{
												if (typeNameEdge.getLabel().equals("_name_")) {
													if (typeNameEdge.getVertex(Direction.IN).getProperty("eObjectID").equals("org.amma.dsl.jdt.dom/SimpleName")) {
														name = typeNameEdge.getVertex(Direction.IN).getProperty("fullyQualifiedName");
														break;
													}
												}
											}
											if (nameVertex.getProperty("fullyQualifiedName").equals(name)) {
												if (result.get(name) == null) {
													result.put(name, new ArrayList<String>());
													result.get(name).add(methodName);
												}
												else {
													result.get(name).add(methodName);
												}
												number++;
												break outerloop;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		long endtime = System.nanoTime();
		for(String key: result.keySet())
		{
			System.out.println(key + ": " + result.get(key));
		}

		System.out.println("Total matches found: " + number);
		
		System.out.println("grabats query took aprox: "
				+ (endtime - startTime) / 1000000 + " milliseconds to run");
	}

}
