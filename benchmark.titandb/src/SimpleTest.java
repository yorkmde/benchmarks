

import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;

public class SimpleTest {

	public static void main(String[] args) {
		String system = System.getProperty("user.dir").replaceAll("\\\\", "/");
		String db = system + "/graph";
		System.out.println(db);
		TitanGraph graph = TitanFactory.open("/tmp/titan");
		for(Vertex v: graph.getVertices())
		{
			graph.removeVertex(v);
		}
		
		for(Edge e: graph.getEdges())
		{
			graph.removeEdge(e);
		}
		
		Vertex v1 = graph.addVertex(null);
		v1.setProperty("name", "William");
		
		Vertex v2 = graph.addVertex(null);
		v2.setProperty("name", "Peter");
		
		Edge e1 = graph.addEdge(null, v2, v1, "knows/s");

		/*
		for(Vertex v: graph.getVertices())
		{
			graph.removeVertex(v);
		}
		for(Edge e: graph.getEdges())
		{
			graph.removeEdge(e);
		}*/
		
		/*
		Vertex v1 = graph.addVertex(null);
		
		Vertex v2 = graph.addVertex(null);
		v2.setProperty("name", "Whoever");
		
		*/
		//graph.commit();
		
		for(Vertex v: graph.getVertices())
		{
			System.out.println(v.getProperty("name"));
			System.out.println("----------");
		}
		for(Edge e: graph.getEdges())
		{
			System.out.println("=======");
			System.out.println(e.getLabel());
		}
		
		graph.commit();
		graph.shutdown();
	}
}
