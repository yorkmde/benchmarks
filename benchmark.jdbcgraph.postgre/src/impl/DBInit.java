package impl;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBInit {
	
	  private static Connection connect = null;
	  private static Statement statement = null;
	  private static ResultSet resultSet = null;


	public static void main(String[] args) throws ClassNotFoundException, SQLException {
	      Class.forName("org.postgresql.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager
	          .getConnection("jdbc:postgresql://localhost/postgres?"
	              + "user=postgres&password=123");

	      // Statements allow to issue SQL queries to the database
	      statement = connect.createStatement();
	      // Result set get the result of the SQL query
	     statement.executeUpdate("drop database set1;");
	      statement.executeUpdate("create database set1;");
	      statement.executeUpdate("ALTER DATABASE blueprints OWNER TO postgres;");
	}
	
}
