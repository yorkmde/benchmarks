package impl;


import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.sql.SqlGraph;

public class TestClass {

	public static void main(String[] args) {
		try {
			  String graphName = "MyFirstGraph";
			  String vertices = "MyVerticesCollection";
			  String edges = "MyEdgesCollection";

			  // cleate a ArangoDB graph
			  Graph graph = new SqlGraph("org.postgresql.Driver", "jdbc:postgresql://localhost/blueprints", "postgres", "123", false);			
		 
			  ((SqlGraph) graph).buildSchema();
			  
			  Vertex v1 =  graph.addVertex(null);

			  v1.setProperty("name", "William's node");
			  
			  System.out.println(v1.getId().toString());
			  Vertex v2 = graph.addVertex(null);

			  Vertex v = graph.getVertices("name", "William's node").iterator().next();
			  
			  System.out.println(v.toString());
			  // create edge
			  Edge e1 = graph.addEdge("e1", v1, v2, "knows");

			  // close the graph
			  graph.shutdown();
			} catch (Exception e) {
			  e.printStackTrace();
			  //fail(e.getMessage());
			}
	}
	
}
